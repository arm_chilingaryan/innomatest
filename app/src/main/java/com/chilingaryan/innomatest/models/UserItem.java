package com.chilingaryan.innomatest.models;

/**
 * Created by chilingaryan on 3/9/18.
 */

public class UserItem {

    String displayName;
    String id;
    String photoUrl;

    public UserItem() {
    }

    public UserItem(String displayName, String id, String photoUrl) {
        this.displayName = displayName;
        this.id = id;
        this.photoUrl = photoUrl;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

}

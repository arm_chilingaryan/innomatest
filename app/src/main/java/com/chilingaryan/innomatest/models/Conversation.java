package com.chilingaryan.innomatest.models;

import java.util.ArrayList;

public class Conversation {
    private ArrayList<Message> mListMessageData;

    public Conversation() {
        mListMessageData = new ArrayList<>();
    }

    public ArrayList<Message> getListMessageData() {
        return mListMessageData;
    }
}
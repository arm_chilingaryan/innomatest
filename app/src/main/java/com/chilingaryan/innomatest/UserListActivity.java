package com.chilingaryan.innomatest;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.chilingaryan.innomatest.adapter.UsersRecyclerAdapter;
import com.chilingaryan.innomatest.models.UserItem;
import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class UserListActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String UNKNOWN = "unknown";

    private RecyclerView mUserRecyclerView;
    private DatabaseReference mDatabase;
    private UsersRecyclerAdapter mUserAdapter;
    private ArrayList<UserItem> mAllUsers = new ArrayList<>();

    private String mCurrentFirebaseID = UNKNOWN;
    private UserItem mCurrentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);

        mUserRecyclerView = findViewById(R.id.users_recycler_view);
        setTitle("Active Users");

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mAllUsers = getAllActiveUser();
        try {
            mCurrentFirebaseID = FirebaseAuth.getInstance().getCurrentUser().getUid();
        } catch (Exception e) {
            e.printStackTrace();
        }
        mUserAdapter = new UsersRecyclerAdapter(mAllUsers, mCurrentUser);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        mUserRecyclerView.setLayoutManager(linearLayoutManager);
        mUserRecyclerView.setAdapter(mUserAdapter);
    }

    private ArrayList<UserItem> getAllActiveUser() {
        mAllUsers.clear();
        mDatabase.child(Constants.USERS_DB_NAME).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    mAllUsers.add(ds.getValue(UserItem.class));
                }

                mUserAdapter.setUserItems(mAllUsers, mCurrentFirebaseID);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        return mAllUsers;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_logout:
                logoutUser();
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    private void logoutUser() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        if (user != null && !TextUtils.isEmpty(user.getUid())) {
            DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference(Constants.USERS_DB_NAME);
            mDatabase.child(user.getUid()).removeValue();
        }

        LoginManager.getInstance().logOut();
        FirebaseAuth.getInstance().signOut();

        Intent intent = new Intent(UserListActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

    }

}

package com.chilingaryan.innomatest.notification;

/**
 * Created by Chilingaryan on 2/14/18.
 */


import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;



/**
 * Created by Ravi Tamada on 08/08/16.
 * www.androidhive.info
 */
public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
    private static final String TAG = MyFirebaseInstanceIDService.class.getSimpleName();

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        // Saving reg id to shared preferences
//        storeRegIdInPref(refreshedToken);

        // sending reg id to your server
        sendRegistrationToServer(refreshedToken);

        // Notify UI that registration has completed, so the progress indicator can be hidden.
        Intent registrationComplete = new Intent(Config.REGISTRATION_COMPLETE);
        registrationComplete.putExtra("token", refreshedToken);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    private void sendRegistrationToServer(final String token) {
        // sending gcm token to server
        Log.e(TAG, "sendRegistrationToServer: " + token);

//        sendIdToServer(token);
    }

//    private void storeRegIdInPref(String token) {
//        KimbiApp.getAppPref().edit().putString(Constants.PUSH_REG_ID, token).apply();
//    }


//    private void sendIdToServer(final String regID) {
//        RetrofitHelper.getCookie(new RetrofitHelper.CookieCallback() {
//            @Override
//            public void onResponse(String cookie) {
//                RetrofitClient.getApiInterface().updateRegID(cookie,regID).enqueue(new Callback<BaseResponse>() {
//                    @Override
//                    public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
//                        BaseResponse updateRegID = response.body();
//                        if (updateRegID != null) {
//
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Call<BaseResponse> call, Throwable t) {
//                        t.printStackTrace();
//
//                    }
//                });
//            }
//        });
//    }

}
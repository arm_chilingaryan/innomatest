package com.chilingaryan.innomatest.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.chilingaryan.innomatest.ChatActivity;
import com.chilingaryan.innomatest.Constants;
import com.chilingaryan.innomatest.R;
import com.chilingaryan.innomatest.models.UserItem;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by chilingaryan on 3/10/18.
 */

public class UsersRecyclerAdapter extends RecyclerView.Adapter<UsersRecyclerAdapter.MyViewHolder> {

    private final DatabaseReference mFirebaseDatabase;
    private List<UserItem> mUserItems;
    //    private List<UserItem> mAllUserItems;
    private Context mContext;
    private UserItem mCurrentUser;
    private ArrayList<String> mCurrentUserFollowList = new ArrayList<>();
//
//    public void setAllUserItems(List<UserItem> allUserItems) {
//        mAllUserItems = allUserItems;
//    }


    public UsersRecyclerAdapter(List<UserItem> userList, UserItem currentUser) {
        mUserItems = userList;
        mCurrentUser = currentUser;

        mFirebaseDatabase = FirebaseDatabase.getInstance().getReference();
        getCurrentUserFollowAndNotify();


    }

    public void setUserItems(List<UserItem> userItems, String currentUserID) {
        for (UserItem userItem : userItems) {
            if (userItem.getId().equals(currentUserID)) {
                mCurrentUser = userItem;
                mUserItems.remove(userItem);
                break;
            }
        }
        notifyDataSetChanged();

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (mContext == null) {
            mContext = parent.getContext();
        }
        View itemView = LayoutInflater.from(mContext)
                .inflate(R.layout.user_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        UserItem userItem = getItem(position);
        holder.userName.setText(userItem.getDisplayName());
        Glide.with(mContext).load(userItem.getPhotoUrl()).into(holder.userProfilePic);

        if (mCurrentUserFollowList.contains(userItem.getId())){
            holder.followImgv.setSelected(true);
        } else {
            holder.followImgv.setSelected(false);
        }
    }

    private UserItem getItem(int position) {
        return mUserItems.get(position);
    }

    @Override
    public int getItemCount() {

        return mUserItems.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final SwipeRevealLayout mSwipeLayout;
        private final LinearLayout layoutHolder;
        TextView userName;
        ImageView userProfilePic;
        private ImageView followImgv;

        MyViewHolder(View itemView) {
            super(itemView);
            layoutHolder = itemView.findViewById(R.id.layout_holder);
            mSwipeLayout = itemView.findViewById(R.id.swipe_layout);
            followImgv = itemView.findViewById(R.id.follow_imgv);
            userName = itemView.findViewById(R.id.user_name_txt);
            userProfilePic = itemView.findViewById(R.id.user_imgView);

            layoutHolder.setOnClickListener(this);
            followImgv.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.layout_holder:
                    UserItem chatItem = getItem(getAdapterPosition());
                    if (mCurrentUser != null) {
                        String roomId = mCurrentUser.getId().compareTo(chatItem.getId()) >0 ? mCurrentUser.getId() + chatItem.getId() : chatItem.getId() + mCurrentUser.getId();
                        Intent intent = new Intent(mContext, ChatActivity.class);
                        intent.putExtra("userId", mCurrentUser.getId());
                        intent.putExtra("friendId", chatItem.getId());
                        intent.putExtra("roomId", roomId);
                        intent.putExtra("nameFriend", chatItem.getDisplayName());
                        mContext.startActivity(intent);
                    }
                    break;

                case R.id.follow_imgv:
                    followImgv.setSelected(!followImgv.isSelected());

                    if (followImgv.isSelected()){
                        mCurrentUserFollowList.add(mUserItems.get(getAdapterPosition()).getId());
                    } else {
                        mCurrentUserFollowList.remove(mUserItems.get(getAdapterPosition()).getId());
                    }
                    mFirebaseDatabase.child(Constants.FOLLOW_DB_NAME).child(mCurrentUser.getId()).setValue(mCurrentUserFollowList);
                    break;

            }
        }
    }

    private void getCurrentUserFollowAndNotify() {

        mFirebaseDatabase.child(Constants.FOLLOW_DB_NAME).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                        if (ds.getKey().equals(mCurrentUser.getId())){
                                mCurrentUserFollowList.clear();
                            GenericTypeIndicator<List<String>> t = new GenericTypeIndicator<List<String>>() {};
                            List<String> tmpFollowList = ds.getValue(t);
                            if (tmpFollowList!=null && !tmpFollowList.isEmpty()){
                                mCurrentUserFollowList.addAll(tmpFollowList);
                                notifyDataSetChanged();
                            }
                        }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}

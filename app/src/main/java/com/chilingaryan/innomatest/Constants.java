package com.chilingaryan.innomatest;

/**
 * Created by chilingaryan on 3/10/18.
 */

public class Constants {
    public static final String USERS_DB_NAME = "users";
    public static final String FOLLOW_DB_NAME = "follow";
    public static final String MESSAGES_DB_NAME = "messages";
}
